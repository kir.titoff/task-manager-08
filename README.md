# TASK-MANAGER

## DEVELOPER

**Name**: Titov Kirill

**E-Mail**: kir.titoff@gmail.com

**E-Mail**: kir.titoff@ya.ru

## SOFTWARE

**Java**: JDK 1.8

**OS**: Windows 10

## HARDWARE

**CPU**: i7

**RAM**: 16 Gb

**SSD**: 512 Gb

## APPLICATION RUN

```bash
java -jar ./task-manager.jar
```
