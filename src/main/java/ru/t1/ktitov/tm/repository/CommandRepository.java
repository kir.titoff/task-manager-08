package ru.t1.ktitov.tm.repository;

import ru.t1.ktitov.tm.api.ICommandRepository;
import ru.t1.ktitov.tm.constant.ArgumentConst;
import ru.t1.ktitov.tm.constant.TerminalConst;
import ru.t1.ktitov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version"
    );

    public static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info"
    );

    public static Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show application commands"
    );

    public static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Exit"
    );

    public static Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show available memory"
    );

    public static Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Show available commands"
    );

    public static Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Show available arguments"
    );

    private static Command[] TERMINAL_COMMANDS = new Command[] {
            VERSION, ABOUT, HELP, INFO, COMMANDS, ARGUMENTS, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
